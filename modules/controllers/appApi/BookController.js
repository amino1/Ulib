const Controller = require('./Controller')
const Book = require(`${config.path.model}/book`);
const User = require(`${config.path.model}/user`);

module.exports = new class SampleController extends Controller {
    index(req , res) {
      Book.find({}, (err, books) => {
        if(err){
          res.json({
            data : err,
            success : false
          });
        }
        res.json({
            data : books,
            success : true
        });
      });
    }

    add(req, res) {
      this.model.Book({
        title : req.body.title,
        author : req.body.author,
        stack : req.body.stack,
        publisher : req.body.publisher,
        publishDate : req.body.publishDate,
        category : req.body.category
      }).save(err => {
          if(err) {
            res.json({
              data : err,
              success : false
            });
          }
          res.json({
              data : 'کتاب با موفقیت ثبت شد',
              success : true
          });
    });
  }

  update(req, res) {
    Book.findById(req.params.id, (err, book) => {
      if(err){
        res.json({
          data : err,
          success : false
        });
      }
      book.stack = req.body.stack;
      book.save(err => {
        if(err){
          res.json({
            data : err,
            success : false
          });
        }
        res.json({
            data : book,
            success : true
        });
      });
    });
  }

  getUser(req, res) {
    User.findById(req.user._id, (err, user) => {
      if(err){
        res.json({
          data : err,
          success : false
        });
      }
      res.json({
        data : user,
        success : true
      });
    });
  }

  searchByTitle(req, res) {
    Book.find({'title': {'$regex': req.params.title, '$options': 'i'}} , (err, books) => {

      if(err){
        res.json({
          data : err,
          success : false
        });
      }
      res.json({
          data : books,
          success : true
      });
    });
  }

  searchByAuthor(req, res) {
    Book.find({author:req.params.author} , (err, books) => {
      if(err){
        res.json({
          data : err,
          success : false
        });
      }
      res.json({
          data : books,
          success : true
      });
    });
  }

  searchByPublisher(req, res) {
    Book.find({publisher:req.params.publisher} , (err, books) => {
      if(err){
        res.json({
          data : err,
          success : false
        });
      }
      res.json({
          data : books,
          success : true
      });
    });
  }

}
