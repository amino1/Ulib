const Controller = require('./Controller')
const User = require(`${config.path.model}/user`);
const Book = require(`${config.path.model}/book`);

module.exports = new class UserController extends Controller{
  favourite(req, res) {
    User.findById(req.user._id, (err, user) => {
      //user.favourites = user.favourites.filter(function(el) { return el._id != req.params.id; });
      var element = user.favourites.find(function(el) { return el._id == req.params.id})
      if (element == null){
        Book.findById(req.params.id, (err, book) => {
          user.favourites.push(book);
          user.save(err => {
            if(err) {
              res.send(err);
            }
            res.json(user);
          })
        });
      } else {
        res.json(user);
      }
    });
  }

  unFavourite(req, res) {
    User.findById(req.user._id, (err, user) => {
      user.favourites = user.favourites.filter(function(el) { return el._id != req.params.id; });
      user.save(err => {
        if(err) {
          res.send(err);
        }
        res.json(user);
      });
    });
  }

  reserve(req, res) {
    User.findById(req.user._id, (err, user) => {
      Book.findById(req.params.id, (err, book) => {
        if(book.stack > 0) {
          user.reserved.push(book);
          user.save(err => {
            if(err) {
              res.send(err);
            }
            book.stack--;
            book.save(err => {
              if(err) {
                res.send(err);
              }
              res.json(user);
            })
          })
        } else {
          res.json({message: "کتاب موجود نیست"});
        }
      })
    });
  }
}
