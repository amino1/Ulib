const express = require('express');
const router = express.Router();

// middlewares
const apiAuth = require('./middleware/apiAuth');

// Controllers
const { controller } = config.path.app;
const AuthController = require(`${controller}/AuthController`);
const BookController = require(`${controller}/BookController`);
const UserController = require(`${controller}/UserController`);

// authentication
router.post('/login' , AuthController.login.bind(AuthController));
router.post('/register' , AuthController.register.bind(AuthController));

// Index
router.get('/' , apiAuth, BookController.index.bind(BookController));

// Add a book
router.post('/book' , apiAuth, BookController.add.bind(BookController));

// Update
router.post('/update/:id' , apiAuth, BookController.update.bind(BookController));

// Get User
router.get('/user' , apiAuth, BookController.getUser.bind(BookController));

// Favourite
router.get('/favourite/:id' , apiAuth, UserController.favourite.bind(UserController));
// unFavourite
router.get('/unfavourite/:id' , apiAuth, UserController.unFavourite.bind(UserController));

// Reserve
router.get('/reserve/:id' , apiAuth, UserController.reserve.bind(UserController));

// Search
router.get('/byauthor/:author' , apiAuth, BookController.searchByAuthor.bind(BookController));
router.get('/bytitle/:title' , apiAuth, BookController.searchByTitle.bind(BookController));
router.get('/bypublisher/:publisher' , apiAuth, BookController.searchByPublisher.bind(BookController));

module.exports = router;
